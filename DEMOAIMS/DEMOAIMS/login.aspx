﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="DEMOAIMS.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style> 
    /*set border to the form*/ 
      
    form { 
        border: 3px solid #f1f1f1; 
    } 
    /*assign full width inputs*/

        input[type=text],
        input[type=password] {
            
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        } 
    /*set a style for the buttons*/

        button {
            text-align: center;
            background-color: #003d7e;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            
        } 
    /* set a hover effect for the button*/ 
      
    button:hover { 
        opacity: 0.8; 
    } 
    /*set extra style for the cancel button*/ 
      
    .cancelbtn { 
        width: auto; 
        padding: 10px 18px; 
        background-color: #f44336; 
    } 
    /*centre the display image inside the container*/ 
      
    .imgcontainer { 
        text-align: center; 
        margin: 24px 0 12px 0; 
    } 
    /*set image properties*/ 
      
    img.avatar { 
        width: 20%; 
        border-radius: 30%; 
    } 
    /*set padding to the container*/ 
      
    .container { 
        padding: 16px; 
    } 
    /*set the forgot password text*/ 
      
    span.psw { 
        float: right; 
        padding-top: 16px; 
    } 
    /*set styles for span and cancel button on small screens*/ 
      
    @media screen and (max-width: 300px) { 
        span.psw { 
            display: block; 
            float: none; 
        } 
        .cancelbtn { 
            width: 100%; 
        } 
    } 
</style> 
    <title>Login</title>
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="background-color:#e2e2e2">

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
         <img src="images/Login_Image.jpg" /alt="Trulli" width="1024" height="949">
    </div>
    <div class="col-sm-4"> 
       <br />
        <br />
        <br />
        <br />
            <br />
        <br />
         <br />
        <br />
        <br />
       
    <form style="border:hidden" runat="server" action="dashboard.aspx"> 
        <div class="imgcontainer"> 
            <img src="Images/oil123.jpg"  
                 alt="Avatar" class="avatar"/> 
            <br />
            <br />
            <br />
            <br />
        </div> 
  
        <div> 
            
           

            <asp:TextBox ID="txtUser" runat="server"  type="text" placeholder="User ID" name="uname" style="font-size:large;background:url(bg-dot.jpg) repeat-x center bottom;
    border:none;
    padding:2px 2px;border-bottom: 1px solid #000000; align-content:center; color:#515466;"></asp:TextBox>
  <br />
            <br />
           
            
          
           
            <asp:TextBox ID="txtPassword" runat="server" type="text" placeholder="Password" name="uname" style="font-size:large; background:url(bg-dot.jpg) repeat-x center bottom;
    border:none;
    padding:2px 2px;border-bottom: 1px solid #000000; color:#515466"></asp:TextBox>
            </div>
  
            <div class="row">
    <div class="col-sm-6">
        
        <asp:CheckBox ID="ckbRemember" runat="server" /> Remember me
         <%--<p><a href="#">Forgot Password?</a></p>--%>
    </div>
                <div class="col-sm-6">
                <p style="text-align:end"><a href="#">Forgot Password?</a></p>
                    </div>
    <%--<div class="col-sm-6 container signin" style="align-content:center"> 
        <p style="text-align:end"><a href="#">Forgot Password?</a></p>
  </div>--%>





             
          
            
            <br /><br />
                <br />
                <br />
                <br />
                <br />
            <button ID="btnLogin" runat="server" type="submit" style= "align-content:center; color:white" >Login</button> 
            <%--<asp:Button ID="btnLogin" runat="server" type="submit" style= "align-content:center; color:white" Text="Login" />--%>

                <div class="container signin" style="align-content:center"> 
                  
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Don't have an account? <a href="#">Create one</a></p>
                   
  </div>
            
        </div> 
  
       <%-- <div style="background-color:#f1f1f1"> 
            <button type="button" class="cancelbtn">Cancel</button> 
            <span class="psw">Forgot <a href="#">password?</a></span> 
        </div> --%>
    </form> 
    </div>
  </div>
</div>
    
</body>
</html>
