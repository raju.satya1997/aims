﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Data;

using System.Data.SqlClient;
using System.Drawing;

namespace DEMOAIMS
{
    public partial class Default1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            string equipno = txtEquipNo.Text;
            decimal yearInService = Convert.ToDecimal(txtYIS.Text);
            decimal nowdia = Convert.ToDecimal(txtNomDia.Text);
            decimal nowthk = Convert.ToDecimal(txtNomThk.Text);
            decimal designtemp = Convert.ToDecimal(txtDesignTemp.Text);
            decimal designpres = Convert.ToDecimal(txtDesignPres.Text);
            decimal theocorrrate = Convert.ToDecimal(txtTheoCorrRate.Text);
            string matstd = txtMaterialStd.Text;
            string matgrade = txtMaterialGrade.Text;
            string injection = txtInjection.Text;
            decimal corrallow = Convert.ToDecimal(txtCorrAllow.Text);
            decimal veryhigh = Convert.ToDecimal(txtVeryHigh.Text);
            decimal high = Convert.ToDecimal(txtHigh.Text);
            decimal med = Convert.ToDecimal(txtMed.Text);

            decimal low = Convert.ToDecimal(txtLow.Text);
            decimal no = Convert.ToDecimal(txtNo.Text);
            decimal yieldstrength = Convert.ToDecimal(txtYS.Text);
            decimal tensilestrength = Convert.ToDecimal(txtTS.Text);
            decimal allowstress = Convert.ToDecimal(txtS.Text);
            decimal efficiencyweld = Convert.ToDecimal(txtE.Text);
            decimal youngsmodulus = Convert.ToDecimal(txtY.Text);
            decimal lmt = Convert.ToDecimal(txtLMT.Text);
            decimal lmy = Convert.ToDecimal(txtLMY.Text);
            decimal lcr = Convert.ToDecimal(txtLCR.Text);
            decimal scr = Convert.ToDecimal(txtSCR.Text);
            decimal prp1 = Convert.ToDecimal(txtPRP1.Text);
            decimal prp2 = Convert.ToDecimal(txtPRP2.Text);
            decimal prp3 = Convert.ToDecimal(txtPRP3.Text);

            decimal pry = 2027;

            decimal oneone = Convert.ToDecimal(1.1);
            decimal six = Convert.ToDecimal(0.6);

            decimal eage = pry - yearInService;
            decimal ethk = lmt;
            decimal mrt = ((designpres * nowdia / 2) / ((allowstress * efficiencyweld) - (six * designpres)));
            decimal ecr;
            if (lcr != 0)
            {
                ecr = lcr;
            }
            else { ecr = theocorrrate; }
             
            decimal art = (ecr * eage / ethk);
            decimal flowstress = ((yieldstrength + tensilestrength) / 2) * oneone * efficiencyweld;
            decimal stengthratio = ((allowstress / 1000) * efficiencyweld * mrt) / (flowstress * ethk);

            // decimal ie1 = (decimal)((double)prp1 * Math.Pow((double)prp1, (double)veryhigh));
            decimal ie1 = (decimal)((double)prp1 * (Math.Pow(0.9, (double)veryhigh)) * Math.Pow(0.7, (double)high) * Math.Pow(0.5, (double)med) * Math.Pow(0.4, (double)low));

            decimal ie2 = (decimal)((double)prp2 * (Math.Pow(0.09, (double)veryhigh)) * Math.Pow(0.2, (double)high) * Math.Pow(0.3, (double)med) * Math.Pow(0.33, (double)low));

            decimal ie3 = (decimal)((double)prp3 * (Math.Pow(0.01, (double)veryhigh)) * Math.Pow(0.1, (double)high) * Math.Pow(0.2, (double)med) * Math.Pow(0.27, (double)low));


            decimal posp1 = ie1 / (ie1 + ie2 + ie3);
            decimal posp2 = ie2 / (ie1 + ie2 + ie3);
            decimal posp3 = ie3 / (ie1 + ie2 + ie3);



            decimal beta11 = (decimal)(Math.Pow(1, 2) * (Math.Pow((double)art, 2)) * (Math.Pow(0.2, 2)) + (Math.Pow(1 - (1 * (double)art), 2)) * (Math.Pow(0.2, 2)) + (Math.Pow((double)stengthratio, 2) * (Math.Pow(0.05, 2))));

            decimal beta12 = (decimal)(Math.Pow(2, 2) * (Math.Pow((double)art, 2)) * (Math.Pow(0.2, 2)) + (Math.Pow(1 - (2 * (double)art), 2)) * (Math.Pow(0.2, 2)) + (Math.Pow((double)stengthratio, 2) * (Math.Pow(0.05, 2))));

            decimal beta13 = (decimal)(Math.Pow(2, 2) * (Math.Pow((double)art, 2)) * (Math.Pow(0.2, 2)) + (Math.Pow(1 - (4 * (double)art), 2)) * (Math.Pow(0.2, 2)) + (Math.Pow((double)stengthratio, 2) * (Math.Pow(0.05, 2))));





            decimal beta1 = (1 - (1 * art) - stengthratio) / (beta11 * beta11);
            decimal beta2 = (1 - (2 * art) - stengthratio) / (beta12 * beta12);
            decimal beta3 = (1 - (4 * art) - stengthratio) / (beta13 * beta13);


            decimal avthk = ethk - mrt;

            decimal rl = avthk / ecr;

            decimal halflife = rl / 2;
            






            txtAvailableThk.Text = Convert.ToDecimal(avthk).ToString();

            txtRemaingLife.Text = Convert.ToDecimal(rl).ToString();

            txtHalfLife.Text = Convert.ToDecimal(halflife).ToString();
            

            //txtOutput.Text = (Convert.ToDecimal(txtPRP1.Text) + Convert.ToDecimal(txtPRP2.Text)).ToString();

            decimal a = (decimal)(NORMSDIST((double)beta1));

            decimal b = (decimal)NORMSDIST((double)beta2);

            decimal c = (decimal)NORMSDIST((double)beta3);

            double damagefactor = ((double)posp1 * (double)a) + ((double)b) + ((double)c) / (0.000156);

            double pof = (0.0000306) * (double)damagefactor;

            txtDF.Text = Convert.ToDouble(damagefactor).ToString();

            txtPOF.Text = Convert.ToDouble(pof).ToString();
            if (Convert.ToDouble(txtAvailableThk.Text) < 0.5)
            {
                txtRank.Text = "A";
                txtRank.ForeColor = Color.White;
                txtRank.BackColor = Color.Red;
            }
            else
            { 
                txtRank.Text = "X";
                txtRank.ForeColor = Color.White;
                txtRank.BackColor = Color.Green;
            }
        

        }



        public static double NORMSDIST(double zValue)
        {
            const double b1 = 0.319381530;
            const double b2 = -0.356563782;
            const double b3 = 1.781477937;
            const double b4 = -1.821255978;
            const double b5 = 1.330274429;
            const double p = 0.2316419;
            const double c = 0.39894228;

            if (zValue >= 0.0)
            {
                double t = 1.0 / (1.0 + p * zValue);
                return (1.0 - c * Math.Exp(-zValue * zValue / 2.0) * t * (t * (t * (t * (t * b5 + b4) + b3) + b2) + b1));
            }
            else
            {
                double t = 1.0 / (1.0 - p * zValue);
                return (c * Math.Exp(-zValue * zValue / 2.0) * t * (t * (t * (t * (t * b5 + b4) + b3) + b2) + b1));
            }
        }

        protected void btnSubmit_Click2(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-V8FO71T\SQLEXPRESS;Initial Catalog=PipeDemo;Integrated Security=SSPI");
            SqlCommand cmd = new SqlCommand();

            SqlParameter parm = new SqlParameter();
            parm.ParameterName = "@Unit_No";
            parm.Value = txtEquipNo.Text;
            try
            {
                con.Open();
                cmd = new SqlCommand("Piping_Module", con);
                da.SelectCommand = cmd;
                cmd.Parameters.Add(parm);
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(ds);
                con.Close();


                if(ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    txtNomDia.Text = Convert.ToString(row["Nom_Dia"]);
                    txtNomThk.Text = Convert.ToString(row["Nom_thk"]);
                    txtDesignPres.Text = Convert.ToString(row["Design_Temp"]);
                    txtDesignTemp.Text = Convert.ToString(row["Design_Pressure"]);
                    txtCorrAllow.Text = Convert.ToString(row["Corrosion_Allowance"]);
                    txtMaterialGrade.Text = Convert.ToString(row["Material_grade"]);
                }
            }
            catch (Exception ex)
            {
            }

            string connetionString = "Data Source=DESKTOP-V8FO71T/SQLEXPRESS;Initial Catalog=PipeDemo; Trusted_Connection=True;Integrated Security=SSPI";
            // [ ] required as your fields contain spaces!!
            string insStmt = "insert into  [dbo].['Pipemast Data table$']([Damage Factor], [POF], [Available Thk], [Remaining Life], [Half Life]) values (@damagefactor,@pof,@avthk,@rl,@halflife)";

            //using (SqlConnection cnn = new SqlConnection(connetionString))
            //{
            //    cnn.Open();
            //    SqlCommand insCmd = new SqlCommand(insStmt, cnn);
            //    // use sqlParameters to prevent sql injection!
            //    insCmd.Parameters.AddWithValue("@damagefactor", txtDF.Text);
            //    insCmd.Parameters.AddWithValue("@pof", txtPOF.Text);
            //    insCmd.Parameters.AddWithValue("@avthk", txtAvailableThk.Text);
            //    insCmd.Parameters.AddWithValue("@rl", txtRemaingLife.Text);
            //    insCmd.Parameters.AddWithValue("@halflife", txtHalfLife.Text);
            //    int affectedRows = insCmd.ExecuteNonQuery();
            //    //MessageBox.Show(affectedRows + " rows inserted!");
            //}






        }







        public class UpdateRecord

        {

            public static void Main()

            {

                string connectionString = "Data Source=localhost;" +

                              "Initial Catalog=Northwind;Integrated Security=SSPI";

                string SQL = "UpdateCategory";



                // Create ADO.NET objects.

                SqlConnection con = new SqlConnection(connectionString);

                SqlCommand cmd = new SqlCommand(SQL, con);

                cmd.CommandType = CommandType.StoredProcedure;



                SqlParameter param;

                param = cmd.Parameters.Add("@Unit_No", SqlDbType.NVarChar, 20);

                param.Value = "Beverages";



                param = cmd.Parameters.Add("@CategoryID", SqlDbType.Int);

                param.Value = 1;



                // Execute the command.

                con.Open();

                int rowsAffected = cmd.ExecuteNonQuery();

                con.Close();



                // Display the result of the operation.

                Console.WriteLine(rowsAffected.ToString() + "");

            }

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }

}