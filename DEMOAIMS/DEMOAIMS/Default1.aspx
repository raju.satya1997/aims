﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default1.aspx.cs" Inherits="DEMOAIMS.Default1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"
   <%-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        input[type=text] {
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
  /*border: 1px solid #555;*/
  outline: none;
}

            input[type=text]:focus {
                background-color: lightblue;
            }

        .button {
            background-color: #003d7e;
            border: none;
            color: white;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display:normal;
            font-size: 16px;
            margin: 4px;
            cursor: pointer;
            border-radius: 4px;
             
        }
        * {box-sizing: border-box;}

body {margin:0;}

.navbar {
  overflow: hidden;
  background-color: #333;
  
  top: 0;
  width: 100%;
}

.navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar a:hover {
  background: #ddd;
  color: black;
}

.main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
        body {
            font-family: "Lato", sans-serif;
        }

        .sidenav {
            height: 100%;
            width: 160px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            padding-top: 20px;
        }

            .sidenav a {
                padding: 6px 8px 6px 16px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
            }

                .sidenav a:hover {
                    color: #f1f1f1;
                }

        .main {
            margin-left: 160px; /* Same as the width of the sidenav */
            font-size: 28px; /* Increased text to enable scrolling */
            padding: 0px 10px;
        }

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


        input[type=text]:focus {
            width: 100%;
        }
    </style>
    <title>Asset Integrity Management System (Piping Module Demo)</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head> 
    <div class="navbar" style="align-items:center">
        <form>
           
  <input type="text" name="search" placeholder="Search..">
</form>
       
</div>
 




<body style=" background-color:#e2e2e2">
    <div class="sidenav">
        <img src="Images/oil123.jpg" / width="100px" height="100px" style="align-content:center" > 
        <br />
        <br />
  <a href="#" style="font-size:16px; color:white">Ammonoa 1</a>
  <a href="#" style="font-size:16px; color:white">Fluids</a>
  <a href="#" style="font-size:16px; color:white">Criticality</a>
  
</div>
    <form id="form1" runat="server">
        <div class="container">
            <div class="py-5 text-center">
            
               <%-- <h1   style="color:#f8d059"><b>Asset Integrity Management System (Piping Module Demo)</b></h1>--%>
                <br />
                <h3 style="color:#003d7e; text-align:start"><b>Equipment Number :</b> </h3>

                <div class="row">
    <div class="col-sm-10">    
        <asp:TextBox class="form-control"  placeholder="enter the equipment number here (601.AL1001)" ID="txtEquipNo" runat="server" Text=""></asp:TextBox>
</div>
    <div class="col-sm-2">
       <asp:Button class="button" id="Button1" type="submit" runat="server" Text="ENTER" OnClick="btnSubmit_Click2"  Width="100%" style="Font-Size:16px" />

    </div>
  </div>


<%--                <asp:TextBox class="form-control"  placeholder="enter the equipment number here (601.AL1001)" ID="txtEquipNo" runat="server" Text=""></asp:TextBox>--%>
                    
                                <%-- <br />--%>
                    
<%--                                 <asp:Button class="button" id="btnEnter" type="submit" runat="server" Text="ENTER" OnClick="btnSubmit_Click2" />--%>
                

               
              
            </div>

            <div class="row">
                <div class="col-md-12 order-md-1">



                    <br />
                    <br />



                    <h4 class="mb-3" style=" color:#003d7e; font-size:16px" ><b>Pipe Master</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="YIS" style="color:#003d7e;font-size:12px; font-weight:normal">Year In service (YIS)</label>
                            <asp:TextBox class="form-control" placeholder="" ID="txtYIS" runat="server" Text="1972"></asp:TextBox>
                            
                        </div>
                        <div class="col-md-6">
                            <label for="NomDia" style="color:#003d7e;font-size:12px; font-weight:normal">Nom Dia (D) inches</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtNomDia" runat="server" Text="97.625"></asp:TextBox>
                            <br />
                            </div>
                        <div class="col-md-6">
                            <label for="NomThk" style="color:#003d7e;font-size:12px; font-weight:normal">Nom thk (NT) inches</label>
                            
                            <asp:TextBox class="form-control"  placeholder="" ID="txtNomThk" runat="server" Text="0.8125"></asp:TextBox>
                           
                             
                        </div>

                        

                        <div class="col-md-6">
                            <label for="DesTemp" style="color:#003d7e;font-size:12px; font-weight:normal">Design Temp degF</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtDesignTemp" runat="server" Text="450"></asp:TextBox>
                            <br />
                        </div>
                        <div class="col-md-6">
                            <label for="DesignPres" style="color: #003d7e;
        font-size: 12px;
        font-weight: normal
    ">Design Pressure (P) Psi</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtDesignPres" runat="server" Text="165"></asp:TextBox>
                           
                            </div>
                        <div class="col-md-6">
                            <label for="TheoCorrRat" style="color:#003d7e;font-size:12px; font-weight:normal">Theoritical Corr Rate</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtTheoCorrRate" runat="server" Text="0.001"></asp:TextBox>
                             
                            <br />
                             
                        </div>



                         <div class="col-md-6">
                            <label for="MatStd" style="color:#003d7e;font-size:12px; font-weight:normal">Material Std</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtMaterialStd" runat="server" Text="A285"></asp:TextBox>
                            
                        </div>
                        <div class="col-md-6">
                            <label for="MatGrade" style="color:#003d7e;font-size:12px; font-weight:normal">Material grade</label>
                           <asp:TextBox class="form-control" placeholder="" ID="txtMaterialGrade" runat="server" Text="C"></asp:TextBox>
                            <br />
                            </div>
                        <div class="col-md-6">
                            <label for="Injection" style="color:#003d7e;font-size:12px; font-weight:normal">Injection points/Intermitent</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtInjection" runat="server" Text="N"></asp:TextBox>
                             
                            
                             
                        </div>

                        <div class="col-md-6">
                            <label for="CorrAllow" style="color:#003d7e;font-size:12px; font-weight:normal">Corrosion Allowance</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtCorrAllow" runat="server" Text="0.125"></asp:TextBox>
                            
                        </div>


                    </div>
                </div>
                






                 <div class="col-md-12 order-md-1">
                    <br />
                     <br />
                     <h4 class="col-mb-3" style="color: #003d7e;
                             font-size: 16px
                     "><b>Inspection Confidence Table</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="VerHigh" style="color:#003d7e;font-size:12px; font-weight:normal">Very High</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtVeryHigh" runat="server" Text="0"></asp:TextBox>
                           
                        </div>
                        <div class="col-md-6">
                            <label for="High" style="color:#003d7e;font-size:12px; font-weight:normal">High</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtHigh" runat="server" Text="1"></asp:TextBox>
                           <br />
                            </div>
                        <div class="col-md-6">
                            <label for="Med" style="color:#003d7e;font-size:12px; font-weight:normal">Med</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtMed" runat="server" Text="0"></asp:TextBox>
                             
                          
                             
                        </div>

                        

                        <div class="col-md-6">
                            <label for="Low" style="color:#003d7e;font-size:12px; font-weight:normal">Low</label>
                            <asp:TextBox class="form-control" placeholder="" ID="txtLow" runat="server" Text="0"></asp:TextBox>
                           <br />
                        </div>
                        <div class="col-md-6">
                            <label for="No" style="color:#003d7e;font-size:12px; font-weight:normal">No</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtNo" runat="server" Text="1"></asp:TextBox>
                           
                            </div>
                       


                    </div>

                   


                   

                </div>









                <div class="col-md-12 order-md-1">
                    <br />
                    <br />
                     <h4 class="col-mb-3" style="color:#003d7e;font-size:16px"><b>Material Table</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="YieldStrength" style="color:#003d7e;font-size:12px; font-weight:normal">Yield Strength (YS) KSI</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtYS" runat="server" Text="30"></asp:TextBox>
                            
                        </div>
                        <div class="col-md-6">
                            <label for="TensileStrength" style="color:#003d7e;font-size:12px; font-weight:normal">Tensile Stregth (TS) Ksi</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtTS" runat="server" Text="60"></asp:TextBox>
                           <br />
                            </div>
                        <div class="col-md-6">
                            <label for="AllowableStress" style="color:#003d7e;font-size:12px; font-weight:normal">Allowable Stress (S) Psi</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtS" runat="server" Text="13750"></asp:TextBox>
                             
                            
                             
                        </div>

                        

                        <div class="col-md-6">
                            <label for="Efficiencyweld" style="color:#003d7e;font-size:12px; font-weight:normal">Efficiency of weld (E)</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtE" runat="server" Text="0.85"></asp:TextBox>
                            <br />
                        </div>
                        <div class="col-md-6">
                            <label for="YoungsModulus" style="color:#003d7e;font-size:12px; font-weight:normal">Youngs Modulus (Y)</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtY" runat="server" Text="1"></asp:TextBox>
                            
                            </div>
                       
                        <br />

                    </div>

                   


                   

                </div>









                <div class="col-md-12 order-md-1">
                    <br />
                    <br />
                     <h4 class="col-mb-3" style="color:#003d7e;font-size:16px"><b>TML table</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="LastMeasuredThk" style="color:#003d7e;font-size:12px; font-weight:normal">Last Measured Thk (LMT)</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtLMT" runat="server" Text="0.75"></asp:TextBox>
                            
                        </div>
                        <div class="col-md-6">
                            <label for="LastMeasuredYear" style="color:#003d7e;font-size:12px; font-weight:normal">Last Measured Year (LMY)</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtLMY" runat="server" Text="2013"></asp:TextBox>
                            <br />
                            </div>
                        <div class="col-md-6">
                            <label for="LongCorrRate" style="color:#003d7e;font-size:12px; font-weight:normal">Long Corr Rate (LCR)</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtLCR" runat="server" Text="0.01"></asp:TextBox>
                            
                            
                            
                        </div>

                        

                        <div class="col-md-6">
                            <label for="ShortCorrRate" style="color:#003d7e;font-size:12px; font-weight:normal">Short Corr Rate (SCR)</label>
                            <asp:TextBox class="form-control" placeholder="" ID="txtSCR" runat="server" Text="0.01"></asp:TextBox>
                           
                        </div>
                        
                       


                    </div>

                   


                   

                </div>








                 <div class="col-md-12 order-md-1">
                    <br />
                     <br />
                    <h4 class="col-mb-3" style="color:#003d7e;font-size:16px"><b>PRP table</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="PRP1" style="color:#003d7e;font-size:12px; font-weight:normal">PRP-1</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtPRP1" runat="server" Text="0.5"></asp:TextBox>
                           
                        </div>
                        <div class="col-md-6">
                            <label for="PRP2" style="color:#003d7e;font-size:12px; font-weight:normal">PRP-2</label>
                           <asp:TextBox class="form-control" placeholder="" ID="txtPRP2" runat="server" Text="0.3"></asp:TextBox>
                            <br />
                            </div>
                        <div class="col-md-6">
                            <label for="PRP3" style="color:#003d7e;font-size:12px; font-weight:normal">PRP-3</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtPRP3" runat="server" Text="0.2"></asp:TextBox>
                             
                            
                             
                        </div>
                         <div class="col-md-6">
                            
                              <asp:Button class="button" id="Button2" type="submit" runat="server" Text="SUBMIT" OnClick="btnSubmit_Click1"  />
              </div>

                   
                 </div>    

                   

                </div>


                 
                
               
                



                 <div class="col-md-12 order-md-1">
                   <br />
                     <br />
                       <h4 class="col-mb-3" style="color:#003d7e;font-size:16px"><b>RESULTS</b></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="DF" style="color:#003d7e;font-size:12px; font-weight:normal">DAMAGE FACTORS</label>
                            <asp:TextBox class="form-control"  placeholder="" ID="txtDF" runat="server"></asp:TextBox>
                            
                        </div>
                        <div class="col-md-6">
                            <label for="POF" style="color: #003d7e;font-size: 12px; font-weight:normal">POF</label>
                           <asp:TextBox class="form-control"  placeholder="" ID="txtPOF" runat="server"></asp:TextBox>
                            <br />
                            </div>
                        <div class="col-md-6">
                            <label for="availablethk" style="color:#003d7e;font-size:12px; font-weight:normal">AVAILABLE thK</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtAvailableThk" runat="server" ></asp:TextBox>
                             
                            
                            
                             
                            
                        </div>
                        <div class="col-md-6">
                            <label for="Remaining Life (RL)" style="color:#003d7e;font-size:12px; font-weight:normal">Remaining Life (RL)</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtRemaingLife" runat="server" ></asp:TextBox>
                             <br />
                            
                        </div>
                        <div class="col-md-6">
                            <label for="HalfLife" style="color: #003d7e; font-size: 12px; font-weight:normal">Half Life</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtHalfLife" runat="server" ></asp:TextBox>
                             
                            
                        </div>
                        <div class="col-md-6">
                            <label for="RANK" style="color:#003d7e;font-size:12px; font-weight:normal">RANK</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtRank" runat="server" ></asp:TextBox>
                             
                            
                        </div>
                        <%--<div class="col-md-4 mb-3">
                            <label for="Overall POF">Overall POF</label>
                            <asp:TextBox  class="form-control"  placeholder="" ID="txtOverallPOF" runat="server"></asp:TextBox>
                             
                            
                        </div>--%>
                        
              </div>

                   
                     

                   

                </div>



            </div>

            <footer class="my-5 pt-5 text-muted text-center text-small">
                <p class="mb-1">© 2020 SenecaGlobal</p>
            </footer>
        </div>
      <%--  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PipeDemoConnectionString %>" SelectCommand="SELECT [Unit_No], [Year_In_service ] AS Year_In_service_, [Design_Temp ] AS Design_Temp_, [Design_Pressure], [Op_Temp ] AS Op_Temp_, [Op_Pressure], [Material_Std], [Material_grade], [Nom_Dia], [Nom_thk] FROM ['Pipemast Data table$']"></asp:SqlDataSource>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" >
           
            <Columns>
                <asp:BoundField DataField="Unit_No" HeaderText="Unit_No" SortExpression="Unit_No"  />
                <asp:BoundField DataField="Year_In_service_" HeaderText="Year_In_service_" SortExpression="Year_In_service_" />
                <asp:BoundField DataField="Design_Temp_" HeaderText="Design_Temp_" SortExpression="Design_Temp_" />
                <asp:BoundField DataField="Design_Pressure" HeaderText="Design_Pressure" SortExpression="Design_Pressure" />
                <asp:BoundField DataField="Op_Temp_" HeaderText="Op_Temp_" SortExpression="Op_Temp_" />
                <asp:BoundField DataField="Op_Pressure" HeaderText="Op_Pressure" SortExpression="Op_Pressure" />
                <asp:BoundField DataField="Material_Std" HeaderText="Material_Std" SortExpression="Material_Std" />
                <asp:BoundField DataField="Material_grade" HeaderText="Material_grade" SortExpression="Material_grade" />
                <asp:BoundField DataField="Nom_Dia" HeaderText="Nom_Dia" SortExpression="Nom_Dia" />
                <asp:BoundField DataField="Nom_thk" HeaderText="Nom_thk" SortExpression="Nom_thk" />
            </Columns>
               
        </asp:GridView>--%>
    </form>
</body>
</html>
