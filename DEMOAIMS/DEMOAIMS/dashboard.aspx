﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="DEMOAIMS.dashboard" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"
   <%-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>

body{
    background: #eee;
}
span{
    font-size:15px;
}
a{
  text-decoration:none; 
  color: #0062cc;
  border-bottom:2px solid #0062cc;
}
.box{
    padding:10px 0px;
}

.box-part{
    background:#FFF;
    border-radius:0;
    padding:20px 10px;
    margin:30px 0px;
}
.text{
    margin:20px 0px;
}

.fa{
     color:#4183D7;
}



        input[type=text] {
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            /*border: 1px solid #555;*/
            outline: none;
        }

            input[type=text]:focus {
                background-color: lightblue;
            }

        .button {
            background-color: #003d7e;
            border: none;
            color: white;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: normal;
            font-size: 16px;
            margin: 4px;
            cursor: pointer;
            border-radius: 4px;
        }
        * {box-sizing: border-box;}

body {margin:0;}

.navbar {
  overflow: hidden;
  background-color: #333;
  align-content:start;
  width: 100%;
}

.navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: end;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar a:hover {
  background: #ddd;
  color: black;
}

.main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
        body {
            font-family: "Lato", sans-serif;
        }

        .sidenav {
            height: 100%;
            width: 160px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            padding-top: 20px;
        }

            .sidenav a {
                padding: 6px 8px 6px 16px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
            }

                .sidenav a:hover {
                    color: #f1f1f1;
                }

        .main {
            margin-left: 160px; /* Same as the width of the sidenav */
            font-size: 28px; /* Increased text to enable scrolling */
            padding: 0px 10px;
        }

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


        input[type=text]:focus {
            width: 100%;
        }
    </style>
    <title>Asset Integrity Management System (Piping Module Demo)</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head> 
    <div class="navbar" style="align-items:center">
        <form runat="server" >
           

            <asp:TextBox runat="server" type="text" name="search" placeholder="Search.."></asp:TextBox>
</form>
       
</div>
 




<body style=" background-color:#e2e2e2">
    <div class="sidenav">
        <img src="Images/oil123.jpg" / width="100px" height="100px" style="align-content:center" > 
        <br />
        <br />
  <a href="#" style="font-size:16px; color:white">Ammonoa 1</a>
  <a href="#" style="font-size:16px; color:white">Fluids</a>
  <a href="#" style="font-size:16px; color:white">Criticality</a>
  
</div>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="box">
    <div class="container">
     	<div class="row">
			 
			    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               
					<div class="box-part text-center">
                        
                       <div id="piechart" style="border:solid"></div>
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                       


<script type="text/javascript">
    // Load google charts
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Equip', 'Damage'],
            ['Piping', 8,],
            ['RVs', 7],
            ['Static Internal', 4],
            ['Static External', 5],
            ['General', 6]
        ]);

        // Optional; add a title and set the width and height of the chart
        var options = { 'title': 'Ammonia 1', 'width': 400, 'height': 400 };

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>
                        
						<div class="title">
							
                            <a href="Default1.aspx" ><h4>Chart 1</h4></a>
						</div>
					 </div>
				</div>	 
				

				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               
					<div class="box-part text-center">
					    
					     <img src="Images/chart.PNG" / width="400px" height="400px" style=" border:solid">
                    
						<div class="title">
							<h4>Chart 2</h4>
						</div>
                        
						
                        
					 </div>
				</div>	 
				
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               
					<div class="box-part text-center">
                        
                        <img src="Images/chart2.PNG" / width="400px" height="400px" style=" border:solid">
                        
						<div class="title">
							<h4>Chart 3</h4>
						</div>
                        
						
                        
					 </div>
				</div>	 
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"">
               
					<div class="box-part text-center">
                        
                       <img src="Images/chart3.PNG" / width="400px" height="400px" style=" border:solid">
                        
						<div class="title">
							<h4>Chart 4</h4>
						</div>
                        
						
                        
					 </div>
				</div>	 
				
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               
					<div class="box-part text-center">
					    
					    <img src="Images/chart4.PNG" / width="400px" height="400px" style=" border:solid">
                    
						<div class="title">
							<h4>Chart 5</h4>
						</div>
                        
						
                        
					 </div>
				</div>	 
				
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               
					<div class="box-part text-center">
                        
                       <img src="Images/chart.PNG" / width="400px" height="400px" style=" border:solid">
                        
						<div class="title">
							<h4>chart 6</h4>
						</div>
                        
						
                        
					 </div>
				</div>
		
		</div>		
    </div>
</div>
 <footer style="color:black; text-align:center">Seneca Global 2020</footer>

</body>
</html>
